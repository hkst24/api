import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(const NetworkingApp());
}

class NetworkingApp extends StatelessWidget {
  const NetworkingApp({super.key});

  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      home: CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(CupertinoIcons.cloud)),
            BottomNavigationBarItem(icon: Icon(CupertinoIcons.star)),
            BottomNavigationBarItem(icon: Icon(CupertinoIcons.news)),
          ],
        ),
        tabBuilder: (BuildContext context, int index) {
          switch (index) {
            case 0:
              return const WeatherPage();
            case 1:
              return const SpacePage();
            case 2:
              return const CurrencyPage();
            default:
              return const WeatherPage();
          }
        },
      ),
    );
  }
}

class WeatherPage extends StatefulWidget {
  const WeatherPage({super.key});

  @override
  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  String cityName = '';
  Future<Map<String, dynamic>> fetchWeatherData() async {
    final response = await http.get(Uri.parse(
        'http://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=528cafcb5e960f3ba37d392ee6d7ae3d&units=metric'));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Ошибка при запросе данных о погоде');
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Погода'),
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(70.0),
            child: CupertinoTextField(
              placeholder: "Введите город",
              onChanged: (value) {
                setState(() {
                  cityName = value;
                });
              },
            ),
          ),
          Expanded(
            child: FutureBuilder<Map<String, dynamic>>(
              future: fetchWeatherData(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('Температура: ${snapshot.data?['main']['temp']}°C',
                            style: const TextStyle(fontSize: 24)),
                        const SizedBox(height: 16),
                        Text('Влажность: ${snapshot.data?['main']['humidity']}%',
                            style: const TextStyle(fontSize: 24)),
                        const SizedBox(height: 16),
                        Text('Скорость ветра: ${snapshot.data?['wind']['speed']} м/с',
                            style: const TextStyle(fontSize: 24)),
                      ],
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                return const CupertinoActivityIndicator();
              },
            ),
          ),
        ],
      ),
    );
  }
}

class SpacePage extends StatefulWidget {
  const SpacePage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _SpacePageState createState() => _SpacePageState();
}

class _SpacePageState extends State<SpacePage> {
  String _imageUrl = '';

  void fetchImageData() async {
    var response = await http.get(Uri.parse('https://api.nasa.gov/planetary/apod?api_key=eDqmhhdvSKQSnK8nyWF9M1DVMEb4hgAcLk2xQPPi'));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      setState(() {
        _imageUrl = data['url'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Картинка космоса'),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _imageUrl == '' ? const CupertinoActivityIndicator() : Image.network(_imageUrl),
            CupertinoButton.filled(
              onPressed: fetchImageData,
              child: const Text('Вывести картинку'),
            ),
          ],
        ),
      ),
    );
  }
}

class CurrencyPage extends StatefulWidget {
  const CurrencyPage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _CurrencyPageState createState() => _CurrencyPageState();
}

class _CurrencyPageState extends State<CurrencyPage> {
  String _usdRate = '';
  String _eurRate = '';
  String _cnyRate = '';

  @override
  void initState() {
    super.initState();
    fetchCurrencyData();
  }

  void fetchCurrencyData() async {
    var response = await http.get(Uri.parse('https://www.cbr-xml-daily.ru/daily_json.js'));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      setState(() {
        _usdRate = data['Valute']['USD']['Value'].toString();
        _eurRate = data['Valute']['EUR']['Value'].toString();
        _cnyRate = data['Valute']['CNY']['Value'].toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Курсы валют'),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Обменный курс USD по ЦБ РФ на сегодня: $_usdRate',
                style: const TextStyle(fontSize: 24)),
            const SizedBox(height: 16),
            Text('Обменный курс EUR по ЦБ РФ на сегодня: $_eurRate',
                style: const TextStyle(fontSize: 24)),
            const SizedBox(height: 16),
            Text('Обменный курс CNY по ЦБ РФ на сегодня: $_cnyRate',
                style: const TextStyle(fontSize: 24)),
          ],
        ),
      ),
    );
  }
}
